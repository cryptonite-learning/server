// This module deals with all database queries.
// The structs database rows will be deserialized into are in the models module.
use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel::result::QueryResult;
use dotenv::dotenv;
use std::env;

use super::models;
use super::schema;

// Get the database URL from the environment
pub fn database_url() -> String {
    // Load the .env file. Environment variables are not static. You have to
    // re-set them when you close your terminal. The .env file solves that
    // problem by providing a file you can use to set them by directory
    // instead.
    dotenv().ok();
    env::var("DATABASE_URL").expect("DATABASE_URL must be set")
}

pub fn establish_connection() -> Result<PgConnection, ConnectionError> {
    PgConnection::establish(&database_url())
}

pub fn insert_paste(conn: &PgConnection, paste: models::NewPaste) -> QueryResult<models::Paste> {
    use schema::pastes::dsl::*;

    // insert the values from the NewPaste struct into the database.
    // The database in responsible for generating the id.
    diesel::insert_into(pastes).values(&paste).get_result(conn)
}

pub fn get_paste(conn: &PgConnection, select_id: uuid::Uuid) -> QueryResult<models::Paste> {
    use schema::pastes::dsl::*;

    // find a paste by uuid (access_key)
    pastes.find(select_id).first(conn)
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::models;
    use crate::test_helpers::*;

    #[test]
    fn test_establish_connection() {
        establish_connection();
    }

    pub fn insert_test_paste(conn: &PgConnection, body: String) -> QueryResult<models::Paste> {
        let paste = models::NewPaste { body: body.clone() };

        insert_paste(conn, paste)
    }

    #[test]
    fn test_insert_get() {
        let connection = connection();
        let body = String::from("TestString");

        let insert_result = insert_test_paste(&connection, body.clone()).unwrap();

        assert_eq!(body, insert_result.body);

        let get_result = get_paste(&connection, insert_result.id).unwrap();

        assert_eq!(get_result, insert_result);
    }

    #[test]
    fn get_unsuccessful() {
        assert_eq!(None, get_paste(&connection(), uuid::Uuid::new_v4()).ok())
    }
}
