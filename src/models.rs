use serde::{Deserialize, Serialize};

use super::schema::pastes;

// Derive trait implementations to make this struct able to be looked up in the
// database and to be serialized to JSON
#[derive(Queryable, PartialEq, Debug, Serialize)]
pub struct Paste {
    // Do not include the id when serializing the struct
    #[serde(skip)]
    pub id: uuid::Uuid,
    #[serde(rename = "text")]
    pub body: String,
}

// This struct should be able to be deserialized and inserted into the database
#[derive(Insertable, Deserialize, PartialEq, Debug)]
// The table to insert into is pastes
#[table_name = "pastes"]
pub struct NewPaste {
    // Deserialize from the field called text into the body field
    #[serde(rename = "text")]
    pub body: String,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct CreateResponse {
    // When serializing and deserializing use the key accessKey
    #[serde(rename = "accessKey")]
    pub access_key: uuid::Uuid,
}

// This is special struct as in that gotham uses it to put url parameters into
// it. In particular this struct is used to contain the access key passed to the
// get paste request.
#[derive(Deserialize, StateData, StaticResponseExtender)]
pub struct GetPathParams {
    pub access_key: uuid::Uuid,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_paste_from_json() {
        let input = "
          {
            \"text\": \"TestString\"
          }
        ";

        let expected = NewPaste {
            body: String::from("TestString"),
        };

        assert_eq!(expected, serde_json::from_str(&input).unwrap());
    }

    #[test]
    fn paste_to_json() {
        let input = Paste {
            id: uuid::Uuid::new_v4(),
            body: String::from("TestString"),
        };

        let expected = "{\"text\":\"TestString\"}";

        assert_eq!(expected, serde_json::to_string(&input).unwrap());
    }

    #[test]
    fn create_response_to_json() {
        let uuid = uuid::Uuid::new_v4();
        let input = CreateResponse { access_key: uuid };

        let expected = format!("{{\"accessKey\":\"{}\"}}", uuid.hyphenated().to_string());

        assert_eq!(expected, serde_json::to_string(&input).unwrap());
    }
}
