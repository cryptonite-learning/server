#[macro_use]
// Diesel is the ORM (Object-Relational-Mapper) this project uses. It helps us
// make requests to the database and turn the results into Rust types.
extern crate diesel;
extern crate dotenv;
extern crate futures;
// Gotham is the web framework we use. It provides means to respond to HTTP
// requests.
extern crate gotham;
#[macro_use]
extern crate gotham_derive;
extern crate gotham_middleware_diesel;
extern crate hyper;
extern crate mime;
// Serde is a package that helps with turning Rust types into JSON and
// vice-versa. (Actually Serde supports more formats than JSON but that's
// the only one this project uses.)
extern crate serde;
// UUID stands for Universally Unique Identifier. These are ids that are
// supposed to be globally unique. We use them instead of enumerated ids
// (1, 2, 3...) in our project so that noone can simply get all pastes
// by trying all numebers.
// This crate helps with dealing with UUIDs
extern crate diesel_migrations;
extern crate uuid;

// In Rust we have to declare our modules a lever higher.
mod database;
mod models;
mod schema;

// This annotation means that the module test_helpers will only be
// build when testing (with `cargo test`).
#[cfg(test)]
mod test_helpers;

use self::diesel_migrations::run_pending_migrations;
use diesel::PgConnection;
use dotenv::dotenv;
use futures::{future, Future, Stream};
use gotham::handler::{HandlerError, HandlerFuture, IntoHandlerError};
use gotham::helpers::http::response::create_response;
use gotham::pipeline::{new_pipeline, single::single_pipeline};
use gotham::router::builder::*;
use gotham::router::Router;
use gotham::state::{FromState, State};
use gotham_middleware_diesel::DieselMiddleware;
use hyper::{Body, StatusCode};
use std::env;
use retry::retry;
use retry::delay::Fibonacci;

// The Gotham Middleware for Diesel provides the Repo type. It's mainly concerned
// with providing a nice interface to a connection pool. The type is
// parameterized over the type of connection to create since we use PostgresQL we
// use PgConnection. This is a type alias for more convenient use.
// We could establish a separate database connection for every request we get,
// but that would be expensive. Instead we create a number of connections when
// the server starts and the request handler borrows them and returns them when
// it's done.
pub type Repo = gotham_middleware_diesel::Repo<PgConnection>;

fn main() {
    dotenv().ok();
    let addr = match env::var("SERVER_URL") {
        Ok(url) => url,
        _ => String::from("127.0.0.1:7878"),
    };

    retry(Fibonacci::from_millis(100), || {
      println!("Running database migrations");
      match database::establish_connection() {
          Ok(conn) => {
              run_pending_migrations(&conn)
                  .expect("Could not run database migrations");
              Ok(())
          },
          Err(e) => {
              println!("Could not run database migrations:{}\nRetrying...", e);
              Err(e)
          },
      }
    }).expect("Fatally could not run the database migrations.");

    println!("Listening at http://{}", addr);
    // Start a new Gotham server. Use the given address and construct a connection
    // pool.
    gotham::start(addr, router(Repo::new(&database::database_url())))
}

// This function constructs a router. The router matches requests to handlers.
// That means it figures out which function (handler) to call for which request.
// It does that mainly by looking at the requested path and the HTTP verb.
fn router(repo: Repo) -> Router {
    // Gotham is a pipeline based framework. That means every request is
    // processed by a number of middlewares. We need only a simple setup
    // with a single pipeline. We use the DieselMiddleware to manage database
    // connections.
    let (chain, pipeline) =
        single_pipeline(new_pipeline().add(DieselMiddleware::new(repo)).build());

    build_router(chain, pipeline, |route| {
        // When a GET request comes in at the root, invoke the
        // hello_world_handler
        route.get("/").to(hello_world_handler);
        // A route scope is a context for which all routes defined within share
        // a common prefix, in this case /v1.
        route.scope("/v1", |route| {
            // delegate POST requests to /v1/pastes to the create_paste_handler
            route.post("/pastes").to(create_paste_handler);
            // delegate GET requests to /v1/pastes/:acces_key to the
            //get_paste_handler
            route
                .get("/pastes/:access_key")
                // This URL contains parameters, the access_key (id) for the
                // requested paste. This line tells Gotham to extract that
                // information into the GetpathParams sturct defined in our
                // models module. See the documentation there.
                .with_path_extractor::<models::GetPathParams>()
                .to(get_paste_handler);
        });
    })
}

const HELLO_WORLD: &str = "Hello World!";

// This is very basic handler. It simply returns the static string
// "Hello World!" defined above.
fn hello_world_handler(state: State) -> (State, &'static str) {
    println!("Handling Hello World request");
    (state, HELLO_WORLD)
}

// This handler replies to requests asking for a certain paste.
// It does not return the Response directly. It returns a Box<HandlerFuture>.
// This seems weird but makes a lot of sense. Processing a request takes
// time. This approach allows the server to accept a number of requests and
// to create futures for handling them. Futures are a way of performing
// asynchronous computation, in this case creating the response is that
// async computation.
fn get_paste_handler(mut state: State) -> Box<HandlerFuture> {
    // Get the access_key passed along in the URL.
    let models::GetPathParams { access_key } = models::GetPathParams::take_from(&mut state);

    // Get the Repo.
    let repo = Repo::borrow_from(&state).clone();

    let f = repo
        // Run the get_paste database query after getting a connection from the
        // repo.
        .run(move |conn| database::get_paste(&conn, access_key))
        // This map_err combinator tells the server what to do in the case of an
        // error.
        .map_err(|e| match e {
            // If the error is a not found error that means no paste was found
            // for the given access key. We return a 404 Not Found status code.
            diesel::result::Error::NotFound => {
                e.into_handler_error().with_status(StatusCode::NOT_FOUND)
            }
            // If the error is not a not found error use the default error
            // handling. (Return a 500 Internal Server Error)
            _ => e.into_handler_error(),
        })
        // This then clause defines what to do with the result of the database
        // query.
        .then(|result| match result {
            Ok(paste) => {
                // We serialize (turn it into JSON) the result with Serde.
                // We expect that to not fail. If it does we crash the server
                // printing a message of what happend.
                let body = serde_json::to_string(&paste).expect("Failed to serialize response");

                // We create a response from that body with a appropriate
                // response code and mime type.
                let res = create_response(&state, StatusCode::OK, mime::APPLICATION_JSON, body);

                future::ok((state, res))
            }
            Err(e) => future::err((state, e)),
        });
    Box::new(f)
}

// This function handles requests to create a new paste.
fn create_paste_handler(mut state: State) -> Box<HandlerFuture> {
    let repo = Repo::borrow_from(&state).clone();
    // We extract the json from the request and create a NewPaste struct. This
    // struct can be deserialized from JSON as defined in our models module.
    let f = extract_json::<models::NewPaste>(&mut state)
        // after deserializing we do..
        .and_then(move |new_paste| {
            // We run the insert_paste database query
            repo.run(move |conn| database::insert_paste(&conn, new_paste))
                .map_err(|e| e.into_handler_error())
        })
        .then(|result| match result {
            // If the database query was successful, ...
            Ok(paste) => {
                // .. we create a sturct called CreateResponse (also defined in
                // our models module).
                let response = models::CreateResponse {
                    access_key: paste.id,
                };

                // We turn that struct into JSON with serde, again crashing the
                // server if that fails.
                let body = serde_json::to_string(&response).expect("Failed to serialize json");

                let res =
                    create_response(&state, StatusCode::CREATED, mime::APPLICATION_JSON, body);
                future::ok((state, res))
            }
            Err(e) => future::err((state, e)),
        });
    Box::new(f)
}

// This is a error handler. Instead of processing normal requests it kicks
// in when something has gone wrong, in this case if the request is malformed.
// If that happens we reply with the status code 400 BAD REQUEST
fn bad_request<E>(e: E) -> HandlerError
where
    E: std::error::Error + Send + 'static,
{
    e.into_handler_error().with_status(StatusCode::BAD_REQUEST)
}

// This is the function we called earlier for extracting the JSON from a request.
fn extract_json<T>(state: &mut State) -> impl Future<Item = T, Error = HandlerError>
where
    T: serde::de::DeserializeOwned,
{
    // We get the body of the request
    Body::take_from(state)
        .concat2()
        // if the fails we reply with bad_request (see above)
        .map_err(bad_request)
        // once we have the body..
        .and_then(|body| {
            let b = body.to_vec();
            // We try to create a string from the bytes
            std::str::from_utf8(&b)
                // If that fails we have bad request, again
                .map_err(bad_request)
                // If it succeedes we deserialize the data with serde and if
                // that fails, again, we reply with bad_request
                .and_then(|s| serde_json::from_str::<T>(s).map_err(bad_request))
        })
}

#[cfg(test)]
mod tests {
    use diesel::result::QueryResult;
    use futures::Future;
    use hyper::StatusCode;
    use std::str;

    use crate::models;
    use crate::test_helpers::*;

    #[test]
    fn hello_world() {
        let response = test_server()
            .client()
            .get("http://localhost")
            .perform()
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);

        let body = response.read_body().unwrap();
        let str_body = str::from_utf8(&body).unwrap();
        let expected = "Hello World!";

        assert_eq!(expected, str_body);
    }

    #[test]
    fn test_create_paste() {
        let body = r#"{"text": "TestString"}"#;
        let server = test_server();

        let create_response = server
            .client()
            .post("http://localhost/v1/pastes", body, mime::APPLICATION_JSON)
            .perform()
            .unwrap();

        assert_eq!(create_response.status(), StatusCode::CREATED);
        let create_body = create_response.read_body().unwrap();
        let create_string_body = String::from_utf8(create_body).unwrap();
        assert!(create_string_body.contains("accessKey"));

        let create_result: models::CreateResponse =
            serde_json::from_str(&create_string_body).unwrap();

        let get_response = server
            .client()
            .get(format!(
                "http://localhost/v1/pastes/{}",
                create_result.access_key
            ))
            .perform()
            .unwrap();

        assert_eq!(get_response.status(), StatusCode::OK);

        let get_body = get_response.read_body().unwrap();
        let get_string_body = String::from_utf8(get_body).unwrap();
        assert!(get_string_body.contains("TestString"));
    }
}
