use diesel::prelude::*;
use diesel_migrations::run_pending_migrations;
use dotenv;
use gotham::test::TestServer;

use crate::router;
use crate::Repo;

pub fn database_url() -> String {
    dotenv::var("DATABASE_URL").expect("DATABASE_URL must be set")
}

pub fn connection() -> PgConnection {
    let conn = PgConnection::establish(&database_url()).unwrap();
    run_pending_migrations(&conn).unwrap();
    conn.begin_test_transaction().unwrap();
    conn
}

pub fn test_repo() -> Repo {
    let conn = PgConnection::establish(&database_url()).unwrap();
    let repo = Repo::with_test_transactions(&database_url());
    run_pending_migrations(&conn).unwrap();
    repo
}

pub fn test_server_from_repo(repo: Repo) -> TestServer {
    TestServer::new(router(repo)).unwrap()
}

pub fn test_server() -> TestServer {
    test_server_from_repo(test_repo())
}
