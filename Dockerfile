# select build image
FROM rust as build

# create a new empty shell project
RUN USER=root cargo new --bin cryptonite
WORKDIR /cryptonite

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# this build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# copy your source tree
COPY ./src ./src
COPY ./migrations ./migrations

# build for release
RUN rm ./target/release/deps/cryptonite*
RUN cargo build --release

# our final base
FROM debian:stretch-slim

# copy the build artifact from the build stage
COPY --from=build /cryptonite/target/release/cryptonite .
COPY --from=build /cryptonite/migrations ./migrations

RUN apt-get update
RUN apt-get install libpq-dev -y

# set the startup command to run your binary
CMD ["./cryptonite"]
