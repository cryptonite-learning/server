-- Your SQL goes here
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE pastes (
  id UUID DEFAULT uuid_generate_v4 (),
  body TEXT NOT NULL,
  PRIMARY KEY (id)
)
