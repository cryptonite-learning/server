# Training Exercise
I wrote this simple server to learn a bit more about Rust, but it can also be seen as a starting point for learning a bit more about web development, in particular front end work. If that sounds good to you, you can do the following exercise.

## The Goal
Write a front end that consumes the API this server provides to implement a client side encrypted pastebin clone.


## Use Cases
You app should cater to two use cases:

### Create an encrypted Paste
The use enters a text to save and a password to encrypt the text with. The text is locally encrypted by a method of your choosing and then send to the server for storage. The server replies with an access key that is then displayed to the user so that he can access the paste later on.

### Accessing a paste
The user enters an access key and a password. The client requests the paste for the access key from the server and locally decrypts it with the provided password. The decrypted text is displayed to the user.

## Questions
Open an issue on this repo or email me at benbals at posteo dot de. My PGP Key is available from [my blog](https://beb.ninja/about/).
