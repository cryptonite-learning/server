with import <nixpkgs> {};

mkShell {
  buildInputs = [
    cargo
    rustracer
    rustup
    rustfmt
    postgresql
    docker-compose
    kcov
  ];
}
