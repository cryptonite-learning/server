[![pipeline status](https://gitlab.com/cryptonite-learning/server/badges/master/pipeline.svg)](https://gitlab.com/cryptonite-learning/server/pipelines)
[![coverage report](https://gitlab.com/cryptonite-learning/server/badges/master/coverage.svg)](https://cryptonite-learning.gitlab.io/server/)


This is a small learning project. I wrote it in Rust to get more comfortable with writing web services in that language.

This is basically a Pastebin clone even though it is a very simple one. You POST a text to it. It stores it in a database and gives you a key with which you can access it. The idea is that the text gets encrypted on the front end such that a password is needed in addition to the access key this server provides.

# Running
You have two options for running this server.

## 1. Docker Compose
This is the easy and convenient option. I recommend it if you want to work against this web server and do the exercise outlined in `doc/training.md`.

Ensure you have Docker ([Linux](https://runnable.com/docker/install-docker-on-linux), [Windows](https://docs.docker.com/docker-for-windows/install/), [Mac](https://docs.docker.com/docker-for-mac/install/)) and [Docker Compose](https://docs.docker.com/compose/install/) installed.

### Prebuilt
You can use the prebuilt containers from our registry to avoid having to do a lot of really expensive Rust compilation. (It takes **minutes**!)
```bash
$ docker-compose -f docker-compose.yml -f docker-compose.prebuilt.yml up
```

### Build your self
If you don't trust us (good instinct!) you can build the docker container locally like so:
```bash
$ docker-compose up
```

Note: If you rerun this command it will not rebuild the container from the local changes you made. To do that you'll have to pass along the `--build` flag, e. g. `$ docker-compose -f stack.yml up --build`.

## 2. Compiling and running locally
This option requires more manual setup, but it enables a faster feedback loop after it has been setup. I would recommend it if you want to hack on the server itself.

1. [Install Rust](https://www.rust-lang.org/tools/install)
2. Ensure you have a postgres database running.
3. Set the environment variable `DATABASE_URL` to point to that database. (Alternatively: Set the URL in the `.env` file to avoid having to reset it after a restart.)
4. Optionally: Test the application `$ cargo test`.
5. Run the server `$ cargo run`.

# API
The API aims to be as simple as possible. Responses are only returned if the provide actual value.

You can use the [Insomnia](https://insomnia.rest/) REST-Client to play around with the API. I provide the meaningful sample requests in `doc/Insomnia_Example_Requests.json`.

## GET /v1/pastes/:access_key
### 200 Ok
```JSON
{
  "text": "..."
}
```

### 404 Not found

## POST /v1/pastes
### Request
```JSON
{
  "text": "..."
}
```

### 201 Created
```JSON
{
  "accessKey": "..."
}
```

### 400 Bad Request
